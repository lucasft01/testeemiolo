##REQUISITOS:
Para a execução do teste é necessário executar cada projeto contido neste commit;

Necessário ter o mongodb instalado na máquina sem autenticação.

---
##PASSOS PARA EXECUÇÃO DO PROJETO 
Execute os projetos na seguinte ordem:

1- Alimente a base de dados com o projeto contido na pasta "capturaDados";

2- Execute a api contida na pasta "api";

3- Execute o frontend contido na pasta "frontend".

---
##OBSERVAÇÕES

Todos os projetos podem ser executados com o comando "npm start" ou "yarn start";

A execução do projeto "capturaDados" é infinita, pois, não foi possível determinar a quantidade de pessoas cadastradas na api.