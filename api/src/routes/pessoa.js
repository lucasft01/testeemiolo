import express from 'express'
const router = express.Router()
import { buscarPessoa, deletarPessoa, alterarPessoa, inserirPessoa } from '../controllers/pessoa'

router.route("/buscarPessoa")
  .post(buscarPessoa)

router.route("/deletarPessoa")
  .post(deletarPessoa)

router.route("/alterarPessoa")
  .post(alterarPessoa)

router.route("/inserirPessoa")
  .post(inserirPessoa)

export default router