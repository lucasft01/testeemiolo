import Pessoa from '../mongo/schemas/pessoa'

export async function buscarPessoa(req, res) {
  try {
    const { name } = req.body
    const pessoa = await Pessoa.findOne({ name })
    if (pessoa)
      res.status(200).send(pessoa)
    else
      res.json({ erroBackend: "Nenhuma pessoa com este nome foi encontrada." })
  } catch (e) {
    res.json({ erroBackend: "Erro no servidor, tente novamente!" })
  }
}

export async function deletarPessoa(req, res) {
  try {
    const { _id } = req.body
    try {
      await Pessoa.findByIdAndRemove({ _id })
      res.sendStatus(200)
    } catch (e) {
      console.log(e)
      res.json({ erroBackend: "Não foi possível deletar a pessoa." })
    }
  } catch (e) {
    console.log(e)
    res.json({ erroBackend: "Erro no servidor, tente novamente!" })
  }
}

export async function alterarPessoa(req, res) {
  try {
    const { dados } = req.body
    await Pessoa.findByIdAndUpdate({ _id: dados._id }, dados)
    res.sendStatus(200)
  } catch (e) {
    console.log(e)
    res.json({ erroBackend: "Erro no servidor, tente novamente!" })
  }
}

export async function inserirPessoa(req, res) {
  try {
    const { dados } = req.body
    const pessoa = await Pessoa.findOne({ name: dados.name })
    if (!pessoa) {
      await Pessoa.insertMany(dados)
      res.sendStatus(200)
    }
    else
      res.json({ erroBackend: "Nome já cadastrado" })
  } catch (e) {
    console.log(e)
    res.json({ erroBackend: "Erro no servidor, tente novamente!" })
  }
}