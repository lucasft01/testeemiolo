import mongoose from 'mongoose'
import BD from './config'
console.log({ BD })
const uri = 'mongodb://localhost:' + BD.porta + '/' + BD.banco;
mongoose.Promise = global.Promise;
mongoose.connect(uri, { useNewUrlParser: true }, (error) => {
  if (error) {
    console.log(error);
  }
});
