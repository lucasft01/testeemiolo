import conexao from './src/mongo/config/conexao'
import express from 'express'
const app = express()
import pessoa from './src/routes/pessoa'
import cors from 'cors';
import bodyParser from 'body-parser';

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/api/pessoaRoutes', pessoa)
app.listen(3015, () => console.log("ok"))
