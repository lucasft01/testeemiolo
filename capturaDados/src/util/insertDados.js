import Pessoa from '../mongo/schemas/pessoa'
import axios from 'axios'
import { runInNewContext } from 'vm';
const urlApi = "https://swapi.co/api/people"

const addDocument = async () => {
  let pessoa = ""
  for (let i = 1; ; i++) {
    try {
      pessoa = await axios.post(`${urlApi}/${i}`)
      pessoa = pessoa.data
      console.log(pessoa)
    } catch (e) {
      i++
      console.log(e)
    }
    await Pessoa.insertMany(pessoa)
  }
}

addDocument()