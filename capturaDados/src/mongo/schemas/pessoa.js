import mongoose from 'mongoose'
const Schema = mongoose.Schema
const pessoaSchema = new Schema(
  {
    name: { type: String, default: "" },
    height: { type: String, default: "" },
    mass: { type: String, default: "" },
    hair_color: { type: String, default: "" },
    skin_color: { type: String, default: "" },
    eye_color: { type: String, default: "" },
    birth_year: { type: String, default: "" },
    gender: { type: String, default: "" },
    url: { type: String, default: "" }
  }
)


pessoaSchema.set('collection', 'pessoa')
const Pessoa = mongoose.model('pessoa', pessoaSchema);

export default Pessoa