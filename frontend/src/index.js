import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';

import App from './App';
import React from 'react';
import thunk from "redux-thunk";
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import reducers from './reducer/reducers';
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import dotenv from "dotenv";
dotenv.config();


const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk))
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));
registerServiceWorker();
