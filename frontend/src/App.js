import React, { Component } from 'react';
import TelaInicial from './components/TelaInicial';

class App extends Component {
  render() {
    return (
      <TelaInicial />
    );
  }
}

export default App;
