import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import FormProcura from '../containers/formProcura/FormProcura';

class TelaInicial extends Component {

  render() {
    return (
      <Grid textAlign="center">
        <Grid.Column width="10" >
          <FormProcura />
        </Grid.Column>
      </Grid>
    )
  }
}

export default TelaInicial