import React from "react";
import { Label } from "semantic-ui-react";

const ErrorInline = ({ text, pointing }) => (
  <Label circular color="red" pointing={(pointing) ? pointing : "above"}>{text}</Label>
);

export default ErrorInline;