import {
  ADD_SUCESSO_FORM, LIMPA_SUCESSO_FORM,
  PREENCHER_FORMULARIO, HANDLE_CHANGE_FORMULARIO,
  ESVAZIAR_FORMULARIO, LIMPA_ERRO_FORM, ADD_ERRO_FORM
} from '../../reducer/type';
import api from '../../api';

export const buscarPessoa = (name) => async dispatch => {
  try {
    const retornoApi = await api.pessoa.buscarPessoa(name)
    dispatch({
      type: ESVAZIAR_FORMULARIO
    })
    if (!retornoApi.erroBackend) {
      dispatch({
        type: PREENCHER_FORMULARIO,
        payload: retornoApi
      })
    } else
      dispatch(addErroForm(retornoApi))
  } catch (e) {
    console.log(e)
    dispatch(addErroForm({ erroBackend: "Não foi possível executar esta operação." }))
  }
}

export const handleChangeFormulario = event => async dispatch => {
  try {
    const nome = event.target.name
    const value = event.target.value
    dispatch({
      type: HANDLE_CHANGE_FORMULARIO,
      payload: { [nome]: value }
    })
    dispatch(limparErro({ [nome]: "" }))
    dispatch(limparErro({ erroBackend: "" }))
  } catch (e) {
    console.log(e)
    dispatch(addErroForm({ erroBackend: "Não foi possível executar esta operação." }))
  }
}

export const deletarPessoa = _id => async dispatch => {
  try {
    const retornoApi = await api.pessoa.deletarPessoa(_id)
    if (!retornoApi.erroBackend) {
      dispatch({
        type: ESVAZIAR_FORMULARIO
      })
      dispatch(addSucessoForm("Pessoa Excluída Com Sucesso!"))
    } else
      dispatch(addErroForm(retornoApi))
  } catch (e) {
    console.log(e)
    dispatch(addErroForm({ erroBackend: "Não foi possível executar esta operação." }))
  }
}

export const alterarPessoa = dados => async dispatch => {
  try {
    const retornoApi = await api.pessoa.alterarPessoa(dados)
    if (!retornoApi.erroBackend) {
      dispatch({
        type: ESVAZIAR_FORMULARIO
      })
      dispatch(addSucessoForm("Pessoa Modificada Com Sucesso!"))
    } else
      dispatch(addErroForm(retornoApi))
  } catch (e) {
    console.log(e)
    dispatch(addErroForm({ erroBackend: "Não foi possível executar esta operação." }))
  }
}

export const inserirPessoa = dados => async dispatch => {
  try {
    const retornoApi = await api.pessoa.inserirPessoa(dados)
    console.log({ retornoApi })
    if (!retornoApi.erroBackend) {
      dispatch({
        type: ESVAZIAR_FORMULARIO
      })
      dispatch(addSucessoForm("Pessoa Inserida Com Sucesso!"))
    }
    else
      dispatch(addErroForm(retornoApi))
  } catch (e) {
    console.log(e)
    dispatch(addErroForm({ erroBackend: "Não foi possível executar esta operação." }))
  }
}
export const addErroForm = (dado) => async dispatch => {
  try {
    dispatch({
      type: ADD_ERRO_FORM,
      payload: dado
    })
  } catch (e) {
    console.log(e)
  }
}
export const limparErro = (nome) => dispatch => {
  try {
    dispatch({
      type: LIMPA_ERRO_FORM,
      payload: nome
    })
  } catch (e) {
    console.log(e)
  }
}

export const addSucessoForm = nome => dispatch => {
  try {
    dispatch({
      type: ADD_SUCESSO_FORM,
      payload: nome
    })
  } catch (e) {
    console.log(e)
  }
}

export const limpaSucessoForm = () => dispatch => {
  try {
    dispatch({
      type: LIMPA_SUCESSO_FORM
    })
  } catch (e) {
    console.log(e)
  }
}