import {
  ADD_ERRO_FORM, ADD_SUCESSO_FORM,
  ESVAZIAR_FORMULARIO, LIMPA_ERRO_FORM,
  PREENCHER_FORMULARIO, HANDLE_CHANGE_FORMULARIO, LIMPA_SUCESSO_FORM
} from '../../reducer/type';

const INITIAL_STATE = {
  nomePessoa: "", _id: "",
  name: "", height: "", mass: "",
  birth_year: "", gender: "", url: "", eye_color: "",
  hair_color: "", skin_color: "", erros: {}, sucesso: ""
}

export default function formProcuraReducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case PREENCHER_FORMULARIO: {
      return { ...state, ...action.payload }
    }
    case ESVAZIAR_FORMULARIO:
      return { ...state, ...INITIAL_STATE }

    case HANDLE_CHANGE_FORMULARIO:
      return { ...state, ...action.payload }

    case ADD_ERRO_FORM:
      return {
        ...state, erros: {
          ...state.erros, ...action.payload
        }
      }
    case LIMPA_ERRO_FORM:
      return {
        ...state, erros: {
          ...state.erros, ...action.payload
        }
      }

    case ADD_SUCESSO_FORM:
      return { ...state, sucesso: action.payload }

    case LIMPA_SUCESSO_FORM:
      return { ...state, sucesso: "" }

    default:
      return { ...state }
  }
}