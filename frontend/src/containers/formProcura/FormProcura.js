import { connect } from 'react-redux';
import React, { Component } from 'react'
import { bindActionCreators } from 'redux';
import ErrorInline from "../../components/ErrorInline";
import { validatorForm } from '../../util/validatorForm';
import { Form, Segment, Input, Label, Button, Grid, Message } from 'semantic-ui-react';
import { buscarPessoa, handleChangeFormulario, deletarPessoa, alterarPessoa, inserirPessoa, addErroForm } from './action';


class FormProcura extends Component {

  submitForm(acao) {
    const {
      _id,
      gender, url, mass,
      name, height, birth_year,
      hair_color, skin_color, eye_color } = this.props.formProcuraReducer

    let dados = {
      gender, url, mass,
      name, height, birth_year,
      hair_color, skin_color, eye_color
    }
    const erros = validatorForm(dados)
    const keyErros = Object.keys(erros)
    keyErros.map(key => this.props.addErroForm({ [key]: erros[key] }))
    if (keyErros.length === 0) {
      switch (acao) {
        case "inserir":
          this.props.inserirPessoa(dados)
          break;
        case "alterar":
          dados._id = _id
          this.props.alterarPessoa(dados)
          break;
        case "deletar":
          this.props.deletarPessoa(_id)
          break;
        default:
          console.log("Error")
          break;
      }
    }
  }



  render() {
    const { nomePessoa, name, height,
      birth_year, gender, url, mass, sucesso,
      hair_color, skin_color, eye_color, erros } = this.props.formProcuraReducer
    return (
      <Segment textAlign="left">
        <Segment.Group style={{ padding: "10px" }}>
          <Form>
            <Grid >
              <Grid.Row>
                <Grid.Column width="8">
                  <Form.Field >
                    <Label>Digite o nome do personagem</Label>
                    <Input name="nomePessoa" value={nomePessoa} onChange={(e) => this.props.handleChangeFormulario(e)} />
                  </Form.Field>
                </Grid.Column>
                <Grid.Column width="8" verticalAlign="bottom">
                  <Button size='medium' onClick={() => this.props.buscarPessoa(nomePessoa)}>Procurar</Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form>
        </Segment.Group>
        <Segment.Group style={{ padding: "10px" }}>
          {erros.erroBackend &&
            <Message negative>
              <Message.Header>{erros.erroBackend}</Message.Header>
            </Message>
          }
          {!!sucesso &&
            <Message positive>
              <Message.Header>{sucesso}</Message.Header>
            </Message>
          }
          <Form>
            <Form.Group widths='equal'>
              <Form.Field error={!!erros.name}>
                <Label>Nome</Label>
                <Input name="name" value={name} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.name && (
                  <ErrorInline text={erros.name} />
                )}
              </Form.Field>
              <Form.Field error={!!erros.height}>
                <Label>Altura</Label>
                <Input name="height" value={height} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.height && (
                  <ErrorInline text={erros.height} />
                )}
              </Form.Field>
              <Form.Field error={!!erros.mass}>
                <Label>Peso</Label>
                <Input name="mass" value={mass} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.mass && (
                  <ErrorInline text={erros.mass} />
                )}
              </Form.Field>
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Field error={!!erros.hair_color}>
                <Label>Cor do Cabelo</Label>
                <Input name="hair_color" value={hair_color} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.hair_color && (
                  <ErrorInline text={erros.hair_color} />
                )}
              </Form.Field>
              <Form.Field error={!!erros.skin_color}>
                <Label>Cor da Pele</Label>
                <Input name="skin_color" value={skin_color} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.skin_color && (
                  <ErrorInline text={erros.skin_color} />
                )}
              </Form.Field>
              <Form.Field error={!!erros.eye_color}>
                <Label>Cor dos Olhos</Label>
                <Input name="eye_color" value={eye_color} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.eye_color && (
                  <ErrorInline text={erros.eye_color} />
                )}
              </Form.Field>
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Field error={!!erros.birth_year}>
                <Label>Data de Nascimento</Label>
                <Input name="birth_year" value={birth_year} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.birth_year && (
                  <ErrorInline text={erros.birth_year} />
                )}
              </Form.Field>
              <Form.Field error={!!erros.gender}>
                <Label>Genero</Label>
                <Input name="gender" value={gender} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.gender && (
                  <ErrorInline text={erros.gender} />
                )}
              </Form.Field>
            </Form.Group>
            <Form.Group>
              <Form.Field error={!!erros.url} width="16">
                <Label>URL de Origem</Label>
                <Input name="url" value={url} onChange={(e) => this.props.handleChangeFormulario(e)} />
                {erros.url && (
                  <ErrorInline text={erros.url} />
                )}
              </Form.Field>
            </Form.Group>
          </Form>
          <Button size='medium' onClick={() => this.submitForm("alterar")}>Alterar</Button>
          <Button size='medium' onClick={() => this.submitForm("deletar")}>Deletar</Button>
          <Button size='medium' onClick={() => this.submitForm("inserir")}>Inserir</Button>
        </Segment.Group>
      </Segment>

    )
  }
}

const mapStateToProps = state => {
  return {
    formProcuraReducer: state.formProcuraReducer
  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    buscarPessoa, handleChangeFormulario,
    deletarPessoa, alterarPessoa, inserirPessoa, addErroForm
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(FormProcura)