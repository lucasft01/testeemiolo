import axios from 'axios';

const enderecoApi = "http://localhost:3015/api"
const msgErroServidor = "Não foi possível se comunicar com o servidor"

export default {
  pessoa: {
    buscarPessoa: async (name) => {
      try {
        const retornoApi = await axios.post(`${enderecoApi}/pessoaRoutes/buscarPessoa`, { name })
        return retornoApi.data
      } catch (e) {
        console.log(e)
        return { erroBackend: msgErroServidor }
      }
    },
    deletarPessoa: async (_id) => {
      try {
        const retornoApi = await axios.post(`${enderecoApi}/pessoaRoutes/deletarPessoa`, { _id })
        return retornoApi
      } catch (e) {
        console.log(e)
        return { erroBackend: msgErroServidor }
      }
    },
    alterarPessoa: async (dados) => {
      try {
        const retornoApi = await axios.post(`${enderecoApi}/pessoaRoutes/alterarPessoa`, { dados })
        return retornoApi
      } catch (e) {
        console.log(e)
        return { erroBackend: msgErroServidor }
      }
    },
    inserirPessoa: async dados => {
      try {
        const retornoApi = await axios.post(`${enderecoApi}/pessoaRoutes/inserirPessoa`, { dados })
        return retornoApi.data
      } catch (e) {
        console.log(e)
        return { erroBackend: msgErroServidor }
      }
    }
  }
}