import { combineReducers } from "redux";
import formProcuraReducer from '../containers/formProcura/reducer';

const reducers = combineReducers({
  formProcuraReducer
});

export default reducers