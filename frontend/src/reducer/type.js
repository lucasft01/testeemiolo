//------------------- FORMULARIO BUSCA -------------------//
export const ADD_ERRO_FORM = "ADD_ERRO_FORM"
export const LIMPA_ERRO_FORM = "LIMPA_ERRO_FORM"
export const ADD_SUCESSO_FORM = "ADD_SUCESSO_FORM"
export const LIMPA_SUCESSO_FORM = "LIMPA_SUCESSO_FORM"
export const ESVAZIAR_FORMULARIO = "ESVAZIAR_FORMULARIO"
export const PREENCHER_FORMULARIO = "PREENCHER_FORMULARIO"
export const HANDLE_CHANGE_FORMULARIO = "HANDLE_CHANGE_FORMULARIO"
export const HANDLE_CHANGE_INPUT_PESQUISA = "HANDLE_CHANGE_INPUT_PESQUISA"