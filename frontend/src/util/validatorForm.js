export function validatorForm(dados) {
  const keysDados = Object.keys(dados)
  let erroFrontEnd = {}
  keysDados.forEach(key => {
    return dados[key] === "" ? erroFrontEnd[key] = "Campo Vazio" : ""
  })

  return erroFrontEnd
}